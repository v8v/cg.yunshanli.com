<?php namespace App\Services\Admin\Product;

use App\Services\Admin\AbstractParam;

/**
 * 文章分类操作有关的参数容器，固定参数，方便分离处理。
 *
 */
class Param extends AbstractParam
{
    protected $id;
    protected $name;
    protected $stock;
    protected $safety_stock;
    protected $classify;
    protected $content;
    protected $product_sn;


    public function setName($name)
    {
        $this->name = $this->attributes['name'] = $name;
        return $this;
    }

    public function setIsActive($is_active)
    {
        $this->is_active = $this->attributes['is_active'] = $is_active;
        return $this;
    }

    public function setId($id)
    {
        $this->id = $this->attributes['id'] = $id;
        return $this;
    }

}
