<?php echo widget('Admin.Common')->header(); ?>
    <?php echo widget('Admin.Common')->top(); ?>
    <?php echo widget('Admin.Menu')->leftMenu(); ?>
    <div class="content">
        <?php echo widget('Admin.Menu')->contentMenu(); ?>
        <?php echo widget('Admin.Common')->crumbs('PurchaseOrder'); ?>
        <div class="main-content">
        <div id="sys-list">
			<div class="row">
				<div class="col-md-12">
					<form method="get" action="" class="form-inline">
						<div class="form-group f-g">
							<label for="search-sn">编号: </label>
							<input type="text" value="<?php if(isset($data['sn'])) echo $data['sn'];?>" name="sn" id="search-sn" class="form-control">
						</div>

						<div class="form-group f-g">
							<label for="search-name">产品名称: </label>
							<input type="text" value="<?php if(isset($data['name'])) echo $data['name'];?>" name="name" id="search-name" class="form-control">
						</div>

						<div class="form-group f-g">
							<label for="search-status">状态: </label>
							<select class="form-control" name="status">
								<option value="">请选择状态</option>
		                        <option value="审批中" >审批中</option>
		                        <option value="已驳回" >已驳回</option>
		                        <option value="已通过" >已通过</option>
		                        <option value="已完成" >已完成</option>
							</select>
						</div>

						<div class="form-group f-g">
							<input class="btn btn-primary" type="submit" value="查询" />
						</div>
					</form>
				</div>
			</div>
          <div class="row">
              <div class=" col-md-12">
                  <div class="panel panel-default">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>编号</th>
                            <th>产品名称</th>
                            <th>库存量</th>
                            <th>安全库存</th>
                            <th>下单数量</th>
                            <th>下单需求</th>
                            <th>出货日</th>
                            <th>状态</th>
                            <th>操作</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($list as $key => $value): ?>
                            <tr>
                              <td><?php echo $value['product_sn']; ?></td>
                              <td><?php echo $value['name']; ?></td>
                              <td><?php echo $value['stock']; ?></td>
                              <td><?php echo $value['safety_stock']; ?></td>
                              <td><?php echo $value['order_number']; ?></td>
                              <td><?php echo $value['order_demand']; ?></td>
                              <td><?php echo $value['out_date']; ?></td>
                              <td><?php echo $value['status']; ?></td>
                              <td>
                                <?php echo widget('Admin.PurchaseOrder')->edit($value); ?>
                                <?php echo widget('Admin.PurchaseOrder')->delete($value); ?>
                              </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                      </div>
                  </div>
              </div>
          </div>
          <?php echo $page; ?>
        </div>
        <?php echo widget('Admin.Common')->footer(); ?>
            
        </div>
    </div>
<?php echo widget('Admin.Common')->htmlend(); ?>
