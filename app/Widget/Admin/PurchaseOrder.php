<?php

namespace App\Widget\Admin;

use App\Widget\Admin\AbstractBase;

/**
 * 产品列表小组件
 */
class PurchaseOrder extends AbstractBase
{
    /**
     * 产品分类列表编辑操作
     *
     * @access public
     */
    public function edit($data)
    {
        $this->setCurrentAction('order', 'edit', 'purchase')->checkPermission();
        $url = R('common', $this->module.'.'.$this->class.'.'.$this->function, ['id' => $data['id']]);
        $html = $this->hasPermission ?
                    '<a href="'.$url.'"><i class="fa fa-pencil"></i></a>'
                        : '<i class="fa fa-pencil" style="color:#ccc"></i>';
        return $html;
    }

    /**
     * 删除操作
     *
     * @access public
     */
    public function delete($data)
    {
        $this->setCurrentAction('order', 'delete', 'purchase')->checkPermission();
        $url = R('common', $this->module.'.'.$this->class.'.'.$this->function, ['id' => $data['id']]);
        $html = $this->hasPermission ?
                    '<a href="javascript:ajaxDelete(\''.$url.'\', \'sys-list\', \'确定吗？\');"><i class="fa fa-trash-o"></i></a>'
                        : '<i class="fa fa-trash-o" style="color:#ccc"></i>';
        return $html;
    }

    /**
     * 面包屑中的按钮
     *
     * @access public
     */
    public function navBtn()
    {
        $this->setCurrentAction('order', 'add', 'purchase')->checkPermission();
        $url = R('common', $this->module.'.'.$this->class.'.'.$this->function);
        $html = $this->hasPermission ?
                    '<div class="btn-group" style="float:right;"><a href="'.$url.'" title="下单" class="btn btn-primary btn-xs"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span>下单</a></div>'
                        : '';
        return $html;
    }

}
