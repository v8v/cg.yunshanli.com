<?php namespace App\Http\Controllers\Admin\Purchase;

use Request, Lang;
use App\Models\Admin\Product as sModel;
use App\Models\Admin\ProductClassify as ClassifyModel;
use App\Services\Admin\Product\Process as sProcess;
use App\Services\Admin\Product\Param as sParam;
use App\Libraries\Js;
use App\Http\Controllers\Admin\Controller;
use App\Services\Admin\Tree;

/**
 * 分类相关
 */
class ProductController extends Controller
{
    /**
     * 显示分类列表
     */
    public function index()
    {
		$where['sn']		= strip_tags(Request::input('sn'));
		$where['name']		= strip_tags(Request::input('name'));
		$where['classify']	= strip_tags(Request::input('classify'));
    	$list = (new sModel())->lists($where);
      //$lists = $list->lists();
//      $list = sModel::all();
 //     exit;
		$class = new ClassifyModel();
      $classify = $class->actClassify();

//		$ttt = $class->getAllChild(12);
//		var_dump($ttt);
	  $select = Tree::dropdownSelect(Tree::genTree($classify));

//      $list = (new sModel())->class;
    	$page = $list->setPath('')->appends(Request::all())->render();
        return view('admin.purchase.product', compact('list', 'page','select'));
    }

    /**
     * 增加
     */
    public function add()
    {
	  if(Request::method() == 'POST') return $this->saveDatasToDatabase();
      $formUrl = R('common', 'purchase.product.add');
      $classify = (new ClassifyModel())->actClassify();
	  $select = Tree::dropdownSelect(Tree::genTree($classify));
      return view('admin.purchase.productadd', compact('formUrl','select'));
    }

    /**
     * 增加保存操作
     *
     * @access private
     */
    private function saveDatasToDatabase()
    {
        $data = (array) Request::input('data');
        $param = new sParam();
        $param->setAttributes($data);
        $manager = new sProcess();
        if($manager->add($param) !== false) return Js::locate(R('common', 'purchase.product.index'), 'parent');
        return Js::error($manager->getErrorMessage());
//        return Js::error($data);
    }

    public function getone()
    {
      $id = Request::input('id');
      $info = (new sModel())->getOneById($id);
      if(empty($info)) return Js::error(Lang::get('common.net_found'));
      return $info;
    }

    /**
     * 编辑文章分类
     */
    public function edit()
    {
    	if(Request::method() == 'POST') return $this->updateDatasToDatabase();
        $id = Request::input('id');
        if( ! $id or ! is_numeric($id)) return Js::error(Lang::get('common.illegal_operation'));
        $info = (new sModel())->getOneById($id);
        if(empty($info)) return Js::error(Lang::get('common.not_found'));
		$classify = new ClassifyModel();
        $select = Tree::dropDownSelect((array) Tree::genTree($classify->actClassify()),$info['pid']);
        $formUrl = R('common', 'purchase.product.edit');
        return view('admin.purchase.productadd', compact('info', 'formUrl', 'id', 'select'));
    }

    /**
     * 编辑文章分类入库处理
     *
     * @access private
     */
    private function updateDatasToDatabase()
    {
        $data = Request::input('data');
        if( ! $data or ! is_array($data)) return Js::error(Lang::get('common.illegal_operation'));
        $param = new sParam();
        $param->setAttributes($data);
        $manager = new sProcess();
        if($manager->edit($param)) return Js::locate(R('common', 'purchase.product.index'), 'parent');
        return Js::error($manager->getErrorMessage());
    }

    /**
     * 删除文章分类
     *
     * @access public
     */
    public function delete()
    {
        if( ! $id = Request::input('id')) return responseJson(Lang::get('common.action_error'));
        if( ! is_array($id)) $id = array($id);
        $manager = new sProcess();
        if($manager->detele($id)) return responseJson(Lang::get('common.action_success'), true);
        return responseJson($manager->getErrorMessage());
    }

}
