<?php

namespace App\Widget\Admin;

use App\Widget\Admin\AbstractBase;

/**
 * 产品分类列表小组件
 */
class ProductClassify extends AbstractBase
{
    /**
     * 产品分类列表编辑操作
     *
     * @access public
     */
    public function edit($data)
    {
        $this->setCurrentAction('classify', 'edit', 'purchase')->checkPermission();
        $url = R('common', $this->module.'.'.$this->class.'.'.$this->function, ['id' => $data['id']]);
        $html = $this->hasPermission ?
                    '<a href="'.$url.'"><i class="fa fa-pencil"></i></a>'
                        : '<i class="fa fa-pencil" style="color:#ccc"></i>';
        return $html;
    }

    /**
     * 文章分类列表删除操作
     *
     * @access public
     */
    public function delete($data)
    {
        $this->setCurrentAction('classify', 'delete', 'purchase')->checkPermission();
        $url = R('common', $this->module.'.'.$this->class.'.'.$this->function, ['id' => $data['id']]);
        $html = $this->hasPermission ?
                    '<a href="javascript:ajaxDelete(\''.$url.'\', \'sys-list\', \'确定吗？\');"><i class="fa fa-trash-o"></i></a>'
                        : '<i class="fa fa-trash-o" style="color:#ccc"></i>';
        return $html;
    }

    /**
     * 面包屑中的按钮
     *
     * @access public
     */
    public function navBtn()
    {
        $this->setCurrentAction('classify', 'add', 'purchase')->checkPermission();
        $url = R('common', $this->module.'.'.$this->class.'.'.$this->function);
        $html = $this->hasPermission ?
                    '<div class="btn-group" style="float:right;"><a href="'.$url.'" title="增加新的产品分类" class="btn btn-primary btn-xs"><span aria-hidden="true" class="glyphicon glyphicon-plus"></span>增加新的产品分类</a></div>'
                        : '';
        return $html;
    }

	/**
	 * 子项目用到的KEY
	 */
	private $son;

	public function classifyList(array $datas, $pid, $prefix = false)
	{
		$html = '';
		if ( ! $this->son) {
			$this->son = \App\Services\Admin\Tree::getSonKey();
		}
		foreach ($datas as $key => $value) {
			if ($prefix === false)
			{
				if($pid != $value['id'] && $pid != 'all') continue;
			}
			$line = ($prefix === false ? '' : $prefix).'|------';
			$html .=view('admin.purchase.classifylist', compact('value', 'prefix'));
			if (isset($value[$this->son]) && is_array($value[$this->son])) {
				$html .= $this->classifyList($value[$this->son], $pid, $line);
			}
		}
		return $html;
	}

}
