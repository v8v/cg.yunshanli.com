<?php

namespace App\Models\Admin;

use App\Models\Admin\Base;
use App\Models\Admin\ProductClassify;
/* ======================================
 * 产品分类模型
 * ====================================== */

class Product extends Base
{
  protected $table = 'product';
//  public $timestamps = true;

  protected $fillable = array('id','classify','name','sort','content','stock','safety_stock','is_delete','created_at','updated_at');

  CONST IS_DELETE_NO  = true;
  CONST IS_DELETE_YES = false;
  CONST IS_ACTIVE_YES = true;

  public function clas()
  {
    return $this->hasOne('App\Models\Admin\ProductClassify');
  }

  /* ======================================
   * 取得未删除的产品分类信息
   * @return array
   * ====================================== */
  public function lists($where = NULL) 
  {
   $query = $this->select('product_classify.name as cname','product_sn','stock','safety_stock','product.name as name','product.id as id');
   $query->leftJoin('product_classify','product.classify','=','product_classify.id');
   $query->orderBy('product.id','desc');
	  $query->where('product.is_delete',self::IS_DELETE_NO);
	  if(isset($where['sn']) and ! empty($where['sn'])) $query->where('product.product_sn',$where['sn']);
	  if(isset($where['name']) and ! empty($where['name'])) $query->where('product.name','like',"%".$where['name']."%");
	  if(isset($where['classify']) and ! empty($where['classify']))
	  {
		$whereIn = (new ProductClassify())->getAllChild($where['classify']); 
		array_unique($whereIn);
		$query->whereIn('product.classify',$whereIn);
	  }
	  $result = $query->paginate(15);
	  return $result;
  }

  public function select_list()
  {
    $list = $this->where('product.is_delete', self::IS_DELETE_NO)->get();
    return $list;
  }


  public function add(array $data) {
    return $this->create($data);
  }

  /* ======================================
    * 修改
    * @param array $data 所需插入的信息
   * ====================================== */
  public function edit(array $data, $id) 
  {
    return $this->where('id','=', intval($id))->update($data);
  }

  /* ======================================
    * 取得指定ID的分类
    * @param intval $id 用户组的ID
    * @return array
   * ====================================== */
  public function getOneById($id) 
  {
    return $this->where('id','=', intval($id))->first();
  }

  /* ======================================
   * 批量删除分类
   * ====================================== */
  public function del(array $data, array $ids)   
  {
    return $this->whereIn('id', $ids)->update($data);
  }


}
