<?php namespace App\Services\Admin\PurchaseOrder;

use Validator, Lang;
use App\Services\Admin\BaseValidate;
use App\Services\Admin\PurchaseOrder\Param as sParam;

/**
 * 用户组列表表单验证
 *
 */
class Validate extends BaseValidate
{
    /**
     * 增加用户组的时候的表单验证
     *
     * @access public
     */
    public function add(sParam $data)
    {
        // 创建验证规则
        $rules = array(
            'product_id' => 'required',
            'order_number'  =>  'required|numeric',
            'out_date'  =>  'required',
            'process_user'  =>  'required',
        );
        
        // 自定义验证消息
        $messages = array(
            'product_id.required' => "请选择产品",
            'order_number.required'=> "下单数量必填",
            'order_number.numeric'=> "下单数量必须为数字",
            'out_date.required'=> "希望出货日必填",
            'process_user.required'=> "审批人必填",
        );
        
        //开始验证
        $validator = Validator::make($data->toArray(), $rules, $messages);
        if($validator->fails())
        {
            $this->errorMsg = $validator->messages()->first();
            return false;
        }
        return true;
    }
    
    /**
     * 编辑用户组的时候的表单验证
     *
     * @access public
     */
    public function edit(sParam $data)
    {
        return $this->add($data);
    }
    
}
