<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductClassifyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_classify', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('pid')->default(0);
            $table->string('name',50);
            $table->boolean('is_active')->default(0);
            $table->boolean('is_delete')->default(0);
            $table->index('pid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_classify');
    }
}
