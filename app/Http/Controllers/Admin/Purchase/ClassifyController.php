<?php namespace App\Http\Controllers\Admin\Purchase;

use Request, Lang;
use App\Models\Admin\ProductClassify as sModel;
use App\Services\Admin\ProductClassify\Process as sProcess;
use App\Services\Admin\ProductClassify\Param as sParam;
use App\Libraries\Js;
use App\Http\Controllers\Admin\Controller;
use App\Services\Admin\Tree;

/**
 * 文章分类相关
 */
class ClassifyController extends Controller
{
    /**
     * 显示分类列表
     */
    public function index()
    {
    	//$list = (new sModel())->unDelClassify();
    	$list = (new sModel())->actClassify();
		$pid = (int) Request::input('pid','all');
//    	$page = $list->setPath('')->appends(Request::all())->render();
		$list = Tree::genTree($list);
//		var_dump($list);
        return view('admin.purchase.classify', compact('list', 'pid'));
    }

    /**
     * 增加文章分类
     */
    public function add()
    {
    	if(Request::method() == 'POST') return $this->saveDatasToDatabase();
        $formUrl = R('common', 'purchase.classify.add');
		$list = (new sModel())->actClassify();
		$select = Tree::dropDownSelect(Tree::genTree($list));
        return view('admin.purchase.classifyadd', compact('formUrl','select'));
    }

    /**
     * 增加分类入库处理
     *
     * @access private
     */
    private function saveDatasToDatabase()
    {
        $data = (array) Request::input('data');
        $param = new sParam();
        $param->setAttributes($data);
        $manager = new sProcess();
        if($manager->addCategory($param) !== false) return Js::locate(R('common', 'purchase.classify.index'), 'parent');
        return Js::error($manager->getErrorMessage());
    }

    /**
     * 编辑分类
     */
    public function edit()
    {
    	if(Request::method() == 'POST') return $this->updateDatasToDatabase();
        $id = Request::input('id');
        if( ! $id or ! is_numeric($id)) return Js::error(Lang::get('common.illegal_operation'));
		$model = new sModel();
        $info = $model->getOneById($id);
        if(empty($info)) return Js::error(Lang::get('common.not_found'));
		$list = (array) Tree::genTree($model->actClassify());
		$select = Tree::dropDownSelect($list, $info['pid']);
        $formUrl = R('common', 'purchase.classify.edit');
        return view('admin.purchase.classifyadd', compact('select', 'info', 'formUrl', 'id'));
    }

    /**
     * 编辑文章分类入库处理
     *
     * @access private
     */
    private function updateDatasToDatabase()
    {
        $data = Request::input('data');
        if( ! $data or ! is_array($data)) return Js::error(Lang::get('common.illegal_operation'));
        $param = new sParam();
        $param->setAttributes($data);
        $manager = new sProcess();
        if($manager->editCategory($param)) return Js::locate(R('common', 'purchase.classify.index'), 'parent');
        return Js::error($manager->getErrorMessage());
    }

    /**
     * 删除文章分类
     *
     * @access public
     */
    public function delete()
    {
        if( ! $id = Request::input('id')) return responseJson(Lang::get('common.action_error'));
        if( ! is_array($id)) $id = array($id);
        $manager = new sProcess();
        if($manager->detele($id)) return responseJson(Lang::get('common.action_success'), true);
        return responseJson($manager->getErrorMessage());
    }

}
