<?php namespace App\Services\Admin\PurchaseOrder;

use App\Services\Admin\AbstractParam;

/**
 * 文章分类操作有关的参数容器，固定参数，方便分离处理。
 *
 */
class Param extends AbstractParam
{
    protected $id;
    protected $product_id;
    protected $order_number;
    protected $order_demand;
    protected $order_user;
    protected $process_user;
    protected $stock;
    protected $safety_stock;
    protected $status;
    protected $out_date;
    protected $remarks;
    protected $is_delete;


}
