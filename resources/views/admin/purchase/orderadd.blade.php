<?php echo widget('Admin.Common')->header(); ?>
    <?php echo widget('Admin.Common')->top(); ?>
    <?php echo widget('Admin.Menu')->leftMenu(); ?>
    <div class="content">
        <?php echo widget('Admin.Menu')->contentMenu(); ?>
        <?php echo widget('Admin.Common')->crumbs(); ?>
        <div class="main-content">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#home" data-toggle="tab">产品信息</a></li>
          </ul>

          <div class="row">
            <div class="col-md-8">
              <br>
              <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="home">
                  <form id="tab" target="hiddenwin" method="post" action="<?php echo $formUrl; ?>">

                    <div class="form-group">
                      <label>下单产品</label><br />
                      <select name="data[product_id]" id="product_id" class="form-control">
                        <option value="">请选择产品</option>
                        <?php if(isset($product) and is_array($product)): ?>
                          <?php foreach($product as $key => $value): ?>
                              <option value="<?php echo $value['id'];?>" <?php if(isset($info['product_id']) && $info['product_id'] == $value['id']) echo 'selected'; ?>><?php echo $value['name'];?></option>
                          <?php endforeach; ?>
                        <?php endif;?>
                      </select>

                    </div>

                    <div class="form-group">
                      <label>库存量</label>
                      <input type="text" id='stock' value="<?php if(isset($info['stock'])) echo $info['stock']; ?>" name="data[stock]" class="form-control">
                    </div>

                    <div class="form-group">
                      <label>安全库存量</label>
                      <input type="text" id='safety_stock' value="<?php if(isset($info['safety_stock'])) echo $info['safety_stock']; ?>" name="data[safety_stock]" class="form-control">
                    </div>

                    <div class="form-group">
                      <label>下单数量</label>
                      <input type="text" value="<?php if(isset($info['order_number'])) echo $info['order_number']; ?>" name="data[order_number]" class="form-control">
                    </div>

                    <div class="form-group">
                      <label>下单需求</label>
                      <input type="text" value="<?php if(isset($info['order_demand'])) echo $info['order_demand']; ?>" name="data[order_demand]" class="form-control">
                    </div>

                    <div class="form-group">
                      <label>希望出货日</label>
                     <input style="width: 150px;" name="data[out_date]" type="text" value="<?php if(isset($info['out_date'])) echo $info['out_date']; ?>" readonly class="form-control form_datetime">
                    </div>

                    <div class="form-group">
                      <label>审批人</label>
                      <select name="data[process_user]" id="DropDownTimezone" class="form-control">
                        <option>请选择审批人</option>
                        <?php if(isset($process_user) and is_array($process_user)): ?>
                          <?php foreach($process_user as $key => $value): ?>
                              <option value="<?php echo $value->id ;?>" <?php if(isset($info['process_user']) && $info['process_user'] == $value->id) echo 'selected'; ?>><?php echo $value->realname;?></option>
                          <?php endforeach; ?>
                        <?php endif;?>
                      </select>
                    </div>
                   

                    <div class="form-group">
                      <label>状态</label>
                      <select name="data[status]" id="DropDownTimezone" class="form-control">
                        <option value="审批中" <?php if(isset($info['status']) && $info['status'] == "审批中") echo 'selected'; ?>>审批中</option>
                        <option value="已驳回" <?php if(isset($info['status']) && $info['status'] == "已驳回") echo 'selected'; ?>>已驳回</option>
                        <option value="已通过" <?php if(isset($info['status']) && $info['status'] == "已通过") echo 'selected'; ?>>已通过</option>
                        <option value="已完成" <?php if(isset($info['status']) && $info['status'] == "已完成") echo 'selected'; ?>>已完成</option>
                      </select>
                    </div>
<?php if(isset($id)):?>
                    <div class="form-group">
                      <label>意见</label>
					  <textarea class="form-control" rows="3" name="data[remarks]" id="remarks"><?php if(isset($info['remarks'])) echo $info['remarks']; ?></textarea>
                   </div>
<?php endif;?>

                    <div class="btn-toolbar list-toolbar">
                      <a class="btn btn-primary sys-btn-submit" data-loading="保存中..." ><i class="fa fa-save"></i> <span class="sys-btn-submit-str">保存</span></a>
                    </div>
                    <?php if(isset($id)): ?>
                      <input name="data[id]" type="hidden" value="<?php echo $id;?>" />
                    <?php endif; ?>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <?php echo widget('Admin.Common')->footer(); ?>
        </div>
    </div>

    <link rel="stylesheet" type="text/css" href="/lib/chosen/min.css">
    <script src="/lib/chosen/min.js" type="text/javascript"></script>
    <script src="/lib/ueditor/ueditor.config.js" type="text/javascript"></script>
    <script src="/lib/ueditor/ueditor.all.min.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="/lib/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <script src="/lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'没有找到！'},
          '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
    </script>
    <script type="text/javascript">
        var ue = UE.getEditor('container', {
          autoHeight: false,
          initialFrameHeight: 500,
          autoFloatEnabled: true
        });

        $(document).keydown(function(e){
          // ctrl + s
          if( e.ctrlKey  == true && e.keyCode == 83 ){
            $('#save-buttom').trigger('click');
            return false; // 截取返回false就不会保存网页了
          }
        });
    </script>

    <script type="text/javascript" charset="utf-8">
   $(".form_datetime").datepicker({format: 'yyyy-mm-dd'});


$(document).ready(function(){
  $("#product_id").change(function(){
    var id = $(this).val();
    $.ajax({
        type: 'get', 
        url: 'purchase-product-getone.html?id='+id,
      //  data: paramObj,
        dataType: 'json',
        success:  function(data) {
          $('#stock').val(data.stock);
          $('#safety_stock').val(data.safety_stock);
//            alertNotic(data.name);
        },
        beforeSend: function() {
          //  var loading = selectObj.attr('data-loading') || 'loading...';
            //selectObj.find('.sys-btn-submit-str').html(loading);
           // selectObj.attr('disabled', 'disabled');
        },
        complete: function() {
           // selectObj.removeAttr('disabled');
           // selectObj.find('.sys-btn-submit-str').html(_oldstr);
        }
    });
});
});
    </script>
<?php echo widget('Admin.Common')->htmlend(); ?>
