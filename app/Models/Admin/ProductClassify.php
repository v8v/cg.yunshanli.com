<?php

namespace App\Models\Admin;

use App\Models\Admin\Base;
/* ======================================
 * 产品分类模型
 * ====================================== */

class ProductClassify extends Base
{
  protected $table = 'product_classify';

  protected $fillable = array('id','pid','name','is_active','is_delete','created_at','updated_at');

  CONST IS_DELETE_NO  = 1;
  CONST IS_DELETE_YES = 0;
  CONST IS_ACTIVE_YES = 1;

  /* ======================================
   * 取得未删除的产品分类信息
   * @return array
   * ====================================== */
  public function unDelClassify() 
  {
    $currentQuery = $this->orderBy('id','desc')->where('is_delete', self::IS_DELETE_NO)->paginate(15);
    return $currentQuery;
  }

  public function actClassify() {
    $currentQuery = $this->orderBy('id','desc')->where('is_delete', self::IS_DELETE_NO)->where('is_active', self::IS_ACTIVE_YES);
    return $currentQuery->get()->toArray();
  }

  public function addClassify(array $data) {
    return $this->create($data);
  }

  /* ======================================
    * 修改产品分类
    * @param array $data 所需插入的信息
   * ====================================== */
  public function editClassify(array $data, $id) 
  {
    return $this->where('id','=', intval($id))->update($data);
  }

  /* ======================================
    * 取得指定ID的分类
    * @param intval $id 用户组的ID
    * @return array
   * ====================================== */
  public function getOneById($id) 
  {
    return $this->where('id','=', intval($id))->first();
  }

  /* ======================================
   * 批量删除分类
   * ====================================== */
  public function delClassify(array $data, array $ids)   
  {
    return $this->whereIn('id', $ids)->update($data);
  }

  public function getParent()
  {

  }
  protected $allChild = array();

	public function getChild($pid = NULL)
	{
		array_push($this->allChild,$pid);
		foreach($this->where('pid',$pid)->get()->toArray() as $k => $v)
		{
			array_push($this->allChild,$v['id']);
			$this->getChild($v['id']);
		}
	}

	public function getAllChild($pid = NULL)
	{	
		$this->allChild = array();
		$this->getChild($pid);
		return $this->allChild;
	}


}
