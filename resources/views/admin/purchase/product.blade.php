<?php echo widget('Admin.Common')->header(); ?>
    <?php echo widget('Admin.Common')->top(); ?>
    <?php echo widget('Admin.Menu')->leftMenu(); ?>
    <div class="content">
        <?php echo widget('Admin.Menu')->contentMenu(); ?>
        <?php echo widget('Admin.Common')->crumbs('Product'); ?>
        <div class="main-content">
        <div id="sys-list">
			<div class="row">
				<div class="col-md-12">
					<form method="get" action="" class="form-inline">
						<div class="form-group f-g">
							<label for="search-sn">编号: </label>
							<input type="text" value="<?php if(isset($data['sn'])) echo $data['sn'];?>" name="sn" id="search-sn" class="form-control">
						</div>

						<div class="form-group f-g">
							<label for="search-name">产品名称: </label>
							<input type="text" value="<?php if(isset($data['name'])) echo $data['name'];?>" name="name" id="search-name" class="form-control">
						</div>

						<div class="form-group f-g">
							<label for="search-status">分类: </label>
                      <select name="classify" id="DropDownTimezone" class="form-control">
						<option value="">请选择分类</option>
						<?php echo $select;?>
                     </select>
						</div>

						<div class="form-group f-g">
							<input class="btn btn-primary" type="submit" value="查询" />
						</div>
					</form>
				</div>
			</div>

          <div class="row">
              <div class=" col-md-12">
                  <div class="panel panel-default">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>编号</th>
                            <th width='50%'>产品名称</th>
                            <th>分类</th>
                            <th>库存量</th>
                            <th>安全库存</th>
                            <th>操作</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($list as $key => $value): ?>
                            <tr>
                              <td><?php echo $value['product_sn']; ?></td>
                              <td><?php echo $value['name']; ?></td>
                              <td><?php echo $value['cname']; ?></td>
                              <td><?php echo $value['stock']; ?></td>
                              <td><?php echo $value['safety_stock']; ?></td>
                              <td>
                                <?php echo widget('Admin.Product')->edit($value); ?>
                                <?php echo widget('Admin.Product')->delete($value); ?>
                              </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                      </div>
                  </div>
              </div>
          </div>
          <?php echo $page; ?>
        </div>
        <?php echo widget('Admin.Common')->footer(); ?>
            
        </div>
    </div>
<?php echo widget('Admin.Common')->htmlend(); ?>
