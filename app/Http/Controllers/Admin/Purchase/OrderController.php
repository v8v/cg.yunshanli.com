<?php namespace App\Http\Controllers\Admin\Purchase;

use DB;
use Request, Lang;
use App\Models\Admin\PurchaseOrder as sModel;
use App\Models\Admin\Product as ProModel;
use App\Services\Admin\PurchaseOrder\Process as sProcess;
use App\Services\Admin\PurchaseOrder\Param as sParam;
use App\Libraries\Js;
use App\Http\Controllers\Admin\Controller;

/**
 * 下单管理
 */
class OrderController extends Controller
{
    /**
     * 显示列表
     */
    public function index()
    {
//      $data = sModel::find(1)->hasOneProduct->toArray();
		//
//      var_dump($data);
		$where['sn']		= strip_tags(Request::input('sn'));
		$where['name']		= strip_tags(Request::input('name'));
		$where['status']	= strip_tags(Request::input('status'));


    	$list = (new sModel())->lists($where);
//      $list = sModel::paginate(15);
    	$page = $list->setPath('')->appends(Request::all())->render();
      return view('admin.purchase.order', compact('list', 'page'));
    }

    /**
     * 增加
     */
    public function add()
    {
    	if(Request::method() == 'POST') return $this->saveDatasToDatabase();
      $formUrl = R('common', 'purchase.order.add');
      $product = (new ProModel())->select_list()->toArray();
      $process_user = DB::table('users')->select('id','realname')->get();
      return view('admin.purchase.orderadd', compact('formUrl','product','process_user'));
    }

    /**
     * 增加保存操作
     *
     * @access private
     */
    private function saveDatasToDatabase()
    {
        $data = (array) Request::input('data');
        $param = new sParam();
        $param->setAttributes($data);
        $manager = new sProcess();
        if($manager->add($param) !== false) return Js::locate(R('common', 'purchase.order.index'), 'parent');
        return Js::error($manager->getErrorMessage());
    }

    /**
     * 编辑文章分类
     */
    public function edit()
    {
    	if(Request::method() == 'POST') return $this->updateDatasToDatabase();
        $id = Request::input('id');
        if( ! $id or ! is_numeric($id)) return Js::error(Lang::get('common.illegal_operation'));
        $info = (new sModel())->getOneById($id);
        if(empty($info)) return Js::error(Lang::get('common.not_found'));
        $product = (new ProModel())->select_list()->toArray();
        $process_user = DB::table('users')->select('id','realname')->get();
        $formUrl = R('common', 'purchase.order.edit');
        return view('admin.purchase.orderadd', compact('info', 'formUrl', 'id', 'process_user','product'));
    }

    /**
     * 编辑文章分类入库处理
     *
     * @access private
     */
    private function updateDatasToDatabase()
    {
        $data = Request::input('data');
        if( ! $data or ! is_array($data)) return Js::error(Lang::get('common.illegal_operation'));
        $param = new sParam();
        $param->setAttributes($data);
        $manager = new sProcess();
        if($manager->edit($param)) return Js::locate(R('common', 'purchase.order.index'), 'parent');
        return Js::error($manager->getErrorMessage());
    }

    /**
     * 删除文章分类
     *
     * @access public
     */
    public function delete()
    {
        if( ! $id = Request::input('id')) return responseJson(Lang::get('common.action_error'));
        if( ! is_array($id)) $id = array($id);
        $manager = new sProcess();
        if($manager->detele($id)) return responseJson(Lang::get('common.action_success'), true);
        return responseJson($manager->getErrorMessage());
    }

}
