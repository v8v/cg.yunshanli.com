<?php

namespace App\Models\Admin;

use App\Models\Admin\Base;
//use App\Models\Admin\ProductClassify;
/* ======================================
 * 产品分类模型
 * ====================================== */

class PurchaseOrder extends Base
{
  protected $table = 'purchase_order';
  public $timestamps = true;

  protected $fillable = array('id','product_id','stock','safety_stock','order_number','order_demand','order_user','process_user','status','out_date','is_delete','remarks');

  CONST IS_DELETE_NO  = true;
  CONST IS_DELETE_YES = false;
  CONST IS_ACTIVE_YES = true;

  public function hasOneProduct()
  {
    return $this->hasOne('App\Models\Admin\Product','id','product_id');
  }

  public function hasOneOrderUser()
  {
    return $this->hasOne('App\Models\Admin\User','id','order_user');
  }


  /* ======================================
   * 取得未删除的产品分类信息
   * @return array
   * ====================================== */
  public function lists($where = NULL) 
  {

	  $query = $this->select('purchase_order.id','purchase_order.order_number','order_demand','p.name','purchase_order.stock','purchase_order.safety_stock','status','out_date','p.product_sn');
	  $query->leftJoin('product as p','product_id','=','p.id');
	  $query->orderBy('id','desc');
	  $query->where('p.is_delete',self::IS_DELETE_NO);
	  if(isset($where['sn']) and ! empty($where['sn'])) $query->where('p.product_sn',$where['sn']);
	  if(isset($where['name']) and ! empty($where['name'])) $query->where('p.name','like',"%".$where['name']."%");
	  if(isset($where['status']) and ! empty($where['status'])) $query->where('purchase_order.status',$where['status']);
	  $result = $query->paginate(15);

//    $currentQuery = $this->select('product_classify.name as cname','stock','safety_stock','product.name as name','product.id as id')->
//      leftJoin('product_classify','product.classify','=','product_classify.id')->
//      orderBy('product.id','desc')->
//      where('product.is_delete', self::IS_DELETE_NO)->
//      paginate(15);
//    $currentQuery = $this->select('purchase_order.id','purchase_order.order_number','order_demand','p.name','purchase_order.stock','purchase_order.safety_stock','status','out_date','p.product_sn')->
//      leftJoin('product as p','product_id','=','p.id')->
//      orderBy('id','desc')->
// //1     where('purchase_order.is_delete', self::IS_DELETE_NO)->
//      where($where)->
//      paginate(15);
    return $result;
  }


  public function add(array $data) {
    return $this->create($data);
  }

  /* ======================================
    * 修改
    * @param array $data 所需插入的信息
   * ====================================== */
  public function edit(array $data, $id) 
  {
    return $this->where('id','=', intval($id))->update($data);
  }

  /* ======================================
    * 取得指定ID的分类
    * @param intval $id 用户组的ID
    * @return array
   * ====================================== */
  public function getOneById($id) 
  {
    return $this->where('id','=', intval($id))->first();
  }

  /* ======================================
   * 批量删除分类
   * ====================================== */
  public function del(array $data, array $ids)   
  {
    return $this->whereIn('id', $ids)->update($data);
   // return $this->whereIn('id', $ids)->delete();
  }


}
