<?php namespace App\Services\Admin\PurchaseOrder;

use Lang;
use App\Models\Admin\PurchaseOrder as sModel;
use App\Services\Admin\PurchaseOrder\Validate as sValidate;
use App\Services\Admin\PurchaseOrder\Param as sParam;
use App\Services\Admin\BaseProcess;

/**
 * 分类处理
 */
class Process extends BaseProcess
{
    /**
     * 分类模型
     * 
     * @var object
     */
    private $sModel;

    /**
     * 分类表单验证对象
     * 
     * @var object
     */
    private $sValidate;
    private $sParam;

    /**
     * 初始化
     *
     * @access public
     */
    public function __construct()
    {
        if( ! $this->sModel) $this->sModel = new sModel();
        if( ! $this->sValidate) $this->sValidate = new sValidate();
        if( ! $this->sParam) $this->sParam = new sParam();
    }

    /**
     * 增加操作
     *
     * @param array $data
     * @access public
     * @return boolean true|false
     */
    public function add(sParam $data)
    {
        if( ! $this->sValidate->add($data)) return $this->setErrorMsg($this->sValidate->getErrorMessage());
        $data = $data->toArray();
        $data['is_delete'] = sModel::IS_DELETE_NO;
        $data['status'] = "审批中";
        if($this->sModel->add($data) !== false) return true;
        return $this->setErrorMsg(Lang::get('common.action_error'));
    }

    /**
     * 删除操作
     * 
     * @param array $ids
     * @access public
     * @return boolean true|false
     */
    public function detele($ids)
    {
        if( ! is_array($ids)) return false;
        $data['is_delete'] = sModel::IS_DELETE_YES;
        if($this->sModel->del($data, $ids) !== false) return true;
        return $this->setErrorMsg(Lang::get('common.action_error'));
    }

    /**
     * 编辑分类
     *
     * @param array $data
     * @access public
     * @return boolean true|false
     */
    public function edit(sParam $data)
    {
        if( ! isset($data['id'])) return $this->setErrorMsg(Lang::get('common.action_error'));
        $id = intval($data['id']); unset($data['id']);
        if( ! $this->sValidate->edit($data)) return $this->setErrorMsg($this->sValidate->getErrorMessage());
        if($this->sModel->edit($data->toArray(), $id) !== false) return true;
        return $this->setErrorMsg(Lang::get('common.action_error'));
    }

}
