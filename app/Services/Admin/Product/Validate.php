<?php namespace App\Services\Admin\Product;

use Validator, Lang;
use App\Services\Admin\BaseValidate;
use App\Services\Admin\Product\Param as sParam;

/**
 * 用户组列表表单验证
 *
 */
class Validate extends BaseValidate
{
    /**
     * 增加用户组的时候的表单验证
     *
     * @access public
     */
    public function add(sParam $data)
    {
        // 创建验证规则
        $rules = array(
            'name' => 'required',
            'stock'  =>  'required|numeric',
            'safety_stock'  =>  'required|numeric',
        );
        
        // 自定义验证消息
        $messages = array(
            'name.required' => "请输入产品名称",
            'stock.required'=> "库存量必填",
            'stock.numeric'=> "库存量必须为数字",
            'safety_stock.required'=> "安全库存必填",
            'safety_stock.numeric'=> "安全库存必须为数字",
        );
        
        //开始验证
        $validator = Validator::make($data->toArray(), $rules, $messages);
        if($validator->fails())
        {
            $this->errorMsg = $validator->messages()->first();
            return false;
        }
        return true;
    }
    
    /**
     * 编辑用户组的时候的表单验证
     *
     * @access public
     */
    public function edit(sParam $data)
    {
        return $this->add($data);
    }
    
}
