<tr>
	<td><span style="color:#ccc;"><?php echo $prefix; ?></span><?php echo $value['name'];?></td>
	<td><?php echo $value['is_active'] == 1 ? '<i class="fa fa-check" style="color:green;"></i>' : '<i class="fa fa-times" style="color:red;"></i>';?></td>
	<td>
		<?php echo widget('Admin.ProductClassify')->edit($value);?>
		<?php echo widget('Admin.ProductClassify')->delete($value);?>
	</td>
</tr>
